from flask import Blueprint, flash, render_template, redirect,abort, request, url_for
site = Blueprint("site",__name__,url_prefix="/",template_folder="../pages/site")

from DBconfig import db
from models.User import User

@site.route("/")
def home():
    db.create_all()
    return render_template("home.html",name="Home")

@site.route("/cadastro",methods=['GET','POST'])
def cadastro():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']

        if not username:
            error = 'Username is required.'
        elif not email:
            error = 'Email is required.'
        elif not password:
            error = 'Password is required'

        if error is None:
            try:
                user = User(username=username,email=email,password=password)
                db.session.add(user)  
                db.session.commit()
                return redirect(url_for('site.users'))  
            except IndexError:
                abort(500)
        
    else:
        return render_template("cadastro.html",name="Cadastro",error=error)
        

@site.route("/users",methods=['GET'])
def users():
    return render_template("users.html",name="Users",users=User.query.all())


@site.route("/delete/<id>")
def delete(id):
    user = User.query.filter_by(id=id).first()
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('site.users'))