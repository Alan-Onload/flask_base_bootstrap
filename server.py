import site
from flask import Flask, render_template
from router.site import site
from router.admin import admin
from DBconfig import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///onload.db'
app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False
app.register_blueprint(site)
app.register_blueprint(admin)
db.init_app(app)

if __name__ == "__main__":
    app.run(debug=True,port=5000)